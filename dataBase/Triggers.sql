/*==============================================================*/
/* Project: Reverso                               				*/
/* Author: Ernestas Cernys										*/
/* Version: final												*/
/* 																*/
/*==============================================================*/

/*==============================================================*/
/* 							Triggers		                    */
/*==============================================================*/

/*==============================================================*/
/* Trigger: Client_id sequence reset                            */
/*==============================================================*/

CREATE OR REPLACE FUNCTION set_client_id_value () RETURNS TRIGGER 
AS $$
BEGIN 
   SELECT setval(Client_client_id_seq, (SELECT MAX(client_id) FROM "Client"));
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS insert_client_max_id ON "Client";

CREATE TRIGGER insert_client_max_id 
BEFORE INSERT ON "Client" 
FOR EACH ROW EXECUTE FUNCTION set_client_id_value();

/*==============================================================*/
/* Trigger: Prospect_id sequence reset                          */
/*==============================================================*/

CREATE OR REPLACE FUNCTION set_prospect_id_value () RETURNS TRIGGER 
AS $$
BEGIN 
   SELECT setval(Prospect_prospect_id_seq, (SELECT MAX(prospect_id) FROM "Prospect"));
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS insert_prospect_max_id ON "Prospect";

CREATE TRIGGER insert_prospect_max_id 
BEFORE INSERT ON "Prospect" 
FOR EACH ROW EXECUTE FUNCTION set_prospect_id_value();

/*==============================================================*/
/* Trigger: Contract_id sequence reset                          */
/*==============================================================*/

CREATE OR REPLACE FUNCTION set_contract_id_value () RETURNS TRIGGER 
AS $$
BEGIN 
   SELECT setval(Contract_contract_id_seq, (SELECT MAX(contract_id) FROM "Contract"));
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS insert_contract_max_id ON "Contract";

CREATE TRIGGER insert_contract_max_id 
BEFORE INSERT ON "Contract" 
FOR EACH ROW EXECUTE FUNCTION set_contract_id_value();
