/*==============================================================*/
/* Project: Reverso                               				*/
/* Author: Ernestas Cernys										*/
/* Version: final												*/
/* 																*/
/*==============================================================*/

/*==============================================================*/
/* 							CREATE TABLES	                    */
/*==============================================================*/
DROP SCHEMA IF EXISTS "public" CASCADE;

CREATE SCHEMA "public";
SET search_path TO "public";

DROP TYPE IF EXISTS "enum_company_domain";
DROP TABLE IF EXISTS "Contract";
DROP TABLE IF EXISTS "Client";
DROP TABLE IF EXISTS "Prospect";


/*==============================================================*/
/* Type: enum_company_domain                                    */
/*==============================================================*/

CREATE TYPE enum_company_domain AS ENUM('Priv�', 'Public');

/*==============================================================*/
/* Type: enum_company_domain                                    */
/*==============================================================*/

CREATE TYPE enum_prospect_interest AS ENUM('Oui', 'Non');


/*==============================================================*/
/* Table: CLIENT                                                */
/*==============================================================*/
CREATE TABLE "Client" 
(
   client_id            SERIAL                        NOT NULL,
   client_full_name     VARCHAR(250)                   NOT NULL,
   client_domain        enum_company_domain            NOT NULL,
   client_street_number VARCHAR(50)                    NOT NULL,
   client_street_name   VARCHAR(250)                   NOT NULL,
   client_postalcode    VARCHAR(250)                   NOT NULL,
   client_city          VARCHAR(250)                   NOT NULL,
   client_phone_number  VARCHAR(20)                    NOT NULL,
   client_email_address VARCHAR(250)                   NOT NULL,
   client_comments      VARCHAR(250)                   NULL,
   client_turnover      INTEGER                        NOT NULL,
   client_employees_count INTEGER                      NOT NULL,
   CONSTRAINT PK_CLIENT PRIMARY KEY (client_id)
);


/*==============================================================*/
/* Table: CONTRACT                                              */
/*==============================================================*/
CREATE TABLE "Contract" 
(
   contract_id          SERIAL                        NOT NULL,
   client_id            INTEGER                        NOT NULL,
   contract_title       VARCHAR(250)                   NOT NULL,
   contract_description VARCHAR(250)                   NOT NULL,
   contract_start_date  DATE                           NOT NULL,
   contract_end_date    DATE                           NULL,
   CONSTRAINT PK_CONTRACT PRIMARY KEY (contract_id),
   CONSTRAINT FK_CONTRACT_CLIENT FOREIGN KEY (client_id)
   		REFERENCES "Client"(client_id) on delete cascade
);

/*==============================================================*/
/* Table: PROSPECT                                              */
/*==============================================================*/
CREATE TABLE "Prospect" 
(
   prospect_id          SERIAL                        NOT NULL,
   prospect_full_name   VARCHAR(250)                   NOT NULL,
   prospect_domain      enum_company_domain            NOT NULL,
   prospect_street_number VARCHAR(50)                 NOT NULL,
   prospect_street_name VARCHAR(250)                   NOT NULL,
   prospect_postalcode  VARCHAR(250)                   NOT NULL,
   prospect_city        VARCHAR(250)                  NOT NULL,
   prospect_phone_number VARCHAR(20)                   NOT NULL,
   prospect_email_address VARCHAR(250)                 NOT NULL,
   prospect_comments    VARCHAR(250)                   NULL,
   prospect_prospection_date DATE                      NOT NULL,
   prospect_interest   enum_prospect_interest          NOT NULL,
   CONSTRAINT PK_PROSPECT PRIMARY KEY (prospect_id)
);

/*==============================================================*/
/* 							INSERTS                             */
/*==============================================================*/

INSERT INTO "Client" (client_id,client_full_name,client_domain,client_street_number, client_street_name, 
	client_postalcode,client_city,client_phone_number,client_email_address,client_turnover,client_employees_count) 
VALUES 
(1,'LEu Euismod Ac LLP','Priv�','803', 'Amet Impasse','Z3854','Gangneung','04 10 25 78 22','eget.nisi@neque.edu',130702,52),
(2,'Blandit Institute','Priv�','397','Arcu. Rd.','00062','Hulste','02 06 34 57 44','nisi@tempor.net',842293,39),
(3,'Rutrum Institute','Public','606','Dictum. Impasse','67000','Hay River','06 06 06 55 47','Nulla@elementumsemvitae.ca',383475,78),
(4,'Aliquam Company','Public','361','Placerat, Route','54898','M�lheim','08 01 27 94 73','lorem.ac@nasceturridiculus.org',577156,61),
(5,'Vulputate Dui Nec Incorporated','Public','996','Dark Rue','22931','Cap-Rouge','09 97 23 61 70','In@felisorciadipiscing.com',72885,82),
(6,'Consequat Nec Mollis Consulting','Public','48','Feugiat Ave','7471','Zhukovsky','01 54 38 49 21','sed.dolor.Fusce@nuncIn.co.uk',400344,94),
(7,'Facilisis Non Bibendum Corporation','Priv�','419','Rhoncus. Route','17744','Valley East','01 46 69 26 45','nibh.vulputate.mauris@velsapien.net',522823,83),
(8,'Pharetra Corp.','Priv�','CP 965', 'Nec Av.','360839','Foz do Igua�u','06 08 33 14 41','ut.pellentesque.eget@sollicitudina.org',830639,21),
(9,'Carson Jackson','Priv�','959','Non Avenue','Z8944','Biggleswade','01 06 55 34 54','fringilla.mi@ornareplacerat.edu',163789,71),
(10,'Carlos Velasquez','Priv�','671','Rhoncus. Avenue','08455','Ucluelet','01 15 88 41 46','arcu.Nunc.mauris@blandit.net',254368,19);

INSERT INTO "Prospect" (prospect_id, prospect_full_name,prospect_domain,prospect_street_number, prospect_street_name, prospect_postalcode,
	prospect_city,prospect_phone_number,prospect_email_address,prospect_prospection_date,prospect_interest) 
VALUES 
(1,'Ad Associates','Public','7683', 'Posuere Avenue','4263','Gu�piles','07 19 27 62 48','enim.mi@Cumsociis.com','2020-08-09','Non'),
(2,'Dictum Mi Ltd','Public','2924', 'Nulla Rd.','88237','Cisano sul Neva','01 23 55 06 07','Vivamus.molestie@ultricessit.com','2020-05-27','Non'),
(3,'Dui Suspendisse Foundation','Priv�','992', 'Non Ave','836296','Afsnee','08 11 93 06 87','tellus.sem@ornareegestasligula.co.uk','2020-04-09','Non'),
(4,'Convallis In Consulting','Priv�','869','Eu Route','N93 1ON','Melilla','07 61 53 07 90','pede.ac.urna@vel.edu','2019-09-04','Non'),
(5,'Pede Cum Sociis LLC','Priv�','266','Lorem Rue','1158','Wadgassen','06 45 30 75 26','tortor.at@blandit.co.uk','2020-06-15','Oui'),
(6,'Ullamcorper Ltd','Priv�','879', 'Urna Impasse','11449','Herselt','08 46 26 96 50','Ut.tincidunt.vehicula@noncursus.edu','2019-10-30','Non'),
(7,'Nulla Tincidunt Institute','Public','111', 'Aenean Avenue','30-235','Anamur','04 51 53 50 12','Donec.vitae@egetvolutpat.ca','2020-11-08','Oui'),
(8,'Lorem Industries','Priv�','CP 454', 'Ipsum Route','26254','G�rouville','01 99 57 52 41','mollis.lectus@nislarcuiaculis.ca','2020-02-27','Oui'),
(9,'In Foundation','Priv�','3150', 'Consectetuer Chemin','151332','Penhold','08 96 03 87 76','sapien.Aenean.massa@morbi.org','2019-05-26','Non'),
(10,'Magna Industries','Public','494', 'Integer Route','62057','Dhule','07 43 60 13 80','justo.sit@Aenean.net','2020-12-22','Non');

INSERT INTO "Contract"(contract_id,client_id,contract_title,contract_description,contract_start_date)VALUES 
(1,3,'STAGE','a odio semper cursus. Integer','2019-07-08'),
(2,10,'STAGE','arcu vel quam dignissim pharetra. Nam ac nulla. In','2020-01-19'),
(3,10,'CDD','elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed','2020-08-22'),
(4,4,'STAGE','scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia.','2019-05-01'),
(5,8,'CDI','magna tellus faucibus leo, in lobortis tellus justo sit amet','2021-01-02'),
(6,6,'CDD','lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis','2020-01-13'),
(7,2,'STAGE','mauris. Suspendisse aliquet molestie tellus.','2020-05-12'),
(8,3,'CDD','Donec felis orci, adipiscing non, luctus sit amet, faucibus ut,','2019-08-26'),
(9,3,'CDI','semper cursus. Integer mollis. Integer','2020-12-24'),
(10,6,'STAGE','velit in aliquet lobortis, nisi nibh','2020-09-24');



