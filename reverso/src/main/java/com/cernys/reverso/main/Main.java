/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.main;

import com.cernys.reverso.dataBase.DAOCommonPostgres;
import com.cernys.reverso.swing.Home;
import javax.swing.JOptionPane;

/**
 * Programme de gestion des clients et prospects
 *
 * @author CDA-25
 */
public class Main {

  /**
   *  Program
   * @param args the command line arguments
   *
   */
  public static void main(String[] args) {
    try {
      Home accueille = new Home();
      accueille.setVisible(true);

    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
    }
  }

}
