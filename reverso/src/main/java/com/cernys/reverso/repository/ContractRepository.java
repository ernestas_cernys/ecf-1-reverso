/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.repository;

import com.cernys.reverso.exceptions.DAO.DAOException;
import com.cernys.reverso.dataBase.DAOContract;
import com.cernys.reverso.entities.Client;
import com.cernys.reverso.entities.Contract;
import com.cernys.reverso.exceptions.entities.EntryException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public class ContractRepository extends DAOContract {

//  private static final Logger LOGGER = LogManager.getLogger(ContractRepository.class.getName());
//
//  private final String FIND_BY_CLIENT = "findContractsByClient";
  //---------------------Method to read all Contracts of a client---------------

//  /**
//   * Method to read all contracts of a client
//   *
//   * @param client
//   * @return
//   * @throws EntryException
//   */
//  public List<Contract> findAllbyClient(Client client) throws EntryException {
//    String query = null;
//    List<Contract> contractList = new ArrayList<>();
//    try {
//      query = getQuery(FIND_BY_CLIENT);
//      Connection connection = getConnection();
//      PreparedStatement statement = connection.prepareStatement(query);
//      System.out.println("Connected to Data base");
//      statement.setInt(1, client.getCompanyID());
//      ResultSet result = statement.executeQuery();
//      while (result.next()) {
//        contractList.add(getContract(result));
//      }
//    } catch (DAOException ex) {
//      LOGGER.fatal("Message: " + ex.getMessage());
//      LOGGER.fatal("StackTrace: " + Arrays.toString(ex.getStackTrace()));
//    } catch (SQLException sqle) {
//      LOGGER.fatal("Message: " + sqle.getMessage()
//              + " [SQL error code : " + sqle.getSQLState() + " ]");
//      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
//      LOGGER.debug("Error while preparing statement: " + query);
//    }
//    return contractList;
//  }
}
