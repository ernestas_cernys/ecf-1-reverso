/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.entities;

import java.io.Serializable;
import com.cernys.reverso.exceptions.entities.EntryException;
import java.util.Comparator;
import java.util.List;

/**
 * Classe de Client qui herrite de la Classe Societé pour gestion de Clients
 *
 * @author CDA-25
 */
public class Client extends Company implements Serializable {
//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Class attributs-------------------------------------
//-----------------Getters and Setters of the Class attributs-------------------
//--------------------------Class methods---------------------------------------  

  /**
   * Sort contracts by contractID
   */
  public static Comparator<Contract> contractComparator = new Comparator<Contract>() {
    @Override
    public int compare(Contract a1, Contract a2) {
      return a1.getContractID().compareTo(a2.getContractID());
    }
  };
//--------------------------Entity attributs------------------------------------ 

  private int turnover;
  private int employeesCount;
  private List<Contract> contracts;

//--------------------------Constructors----------------------------------------  
  /**
   * Empty constructor
   */
  public Client() {
  }

  /**
   * Client constructor
   *
   * @param pTurnover
   * @param pEmployeesCount
   * @param pCompanyName
   * @param pDomain
   * @param pStreetNumber
   * @param pStreetName
   * @param pPostalcode
   * @param pCity
   * @param pPhoneNumber
   * @param pEmailAddress
   * @throws EntryException
   */
  public Client(int pTurnover, int pEmployeesCount, String pCompanyName, Domain pDomain, String pStreetNumber,
          String pStreetName, String pPostalcode, String pCity, String pPhoneNumber,
          String pEmailAddress) throws EntryException {
    super(pCompanyName, pDomain, pStreetNumber, pStreetName, pPostalcode,
            pCity, pPhoneNumber, pEmailAddress);
    setTurnover(pTurnover);
    setEmployeesCount(pEmployeesCount);
    checkTurnover(pTurnover, pEmployeesCount);
  }
//--------------------------Entity public methods-------------------------------

  /**
   * Turnover and number of employees ratio check
   *
   * @param pTurnover
   * @param pEmployeesCount Turnover and number of employees ratio can't be less
   * than 11
   * @throws EntryException
   */
  public void checkTurnover(int pTurnover, int pEmployeesCount) throws EntryException {
    if ((pEmployeesCount <= 0) || (pTurnover / pEmployeesCount) <= 10) {
      throw new EntryException("Turnover and number of employees ratio can't "
              + "be inferieur to 10, recieved: turnover: \"" + pTurnover +
              "\", EmployeesCount: \"" + pEmployeesCount + "\"");
    }
  }

//--------------------------Entity protected methods----------------------------
//--------------------------Entity private methods------------------------------
//--------------------------Entity abstract methods-----------------------------
//-----------------Gettters and Setters of the Entity attributs-----------------  
  /**
   * @return the turnover
   */
  public int getTurnover() {
    return turnover;
  }

  /**
   * @return the employeesCount
   */
  public int getEmployeesCount() {
    return employeesCount;
  }

  /**
   * @param pTurnover the turnover to set Turnover can't be inferieur to 0
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setTurnover(int pTurnover) throws EntryException {
    if (pTurnover < 0) {
      throw new EntryException("Turnover can't be inferieur to 0, recieved: \"" + 
              pTurnover + "\"");
    }
    this.turnover = pTurnover;
  }

  /**
   * @param pEmployeesCount the employeesCount to set Number of employees can't
   * be inferieur or equal to 0
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setEmployeesCount(int pEmployeesCount) throws EntryException {
    if (pEmployeesCount <= 0) {
      throw new EntryException("Number of employees can't be inferieur or equal to 0,"
              + "recieved: \"" + pEmployeesCount + "\"");
    }
    this.employeesCount = pEmployeesCount;
  }

  /**
   * @return the contracts
   */
  public List<Contract> getContracts() {
    contracts.sort(contractComparator);
    return contracts;
  }

  /**
   * @param contrats the contracts to set
   */
  public void setContracts(List<Contract> contrats) {
    this.contracts = contrats;
  }
}
