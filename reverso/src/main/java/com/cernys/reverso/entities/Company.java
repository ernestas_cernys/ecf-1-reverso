/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.entities;

import java.io.Serializable;
import java.util.Comparator;
import com.cernys.reverso.exceptions.entities.EntryException;

/**
 * Abstract Class of Company
 *
 * @author CDA-25
 */
public abstract class Company implements Serializable {
//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Class attributs-------------------------------------  
//------------Getters and Setters of Class attributs----------------------------  
//--------------------------Class methods---------------------------------------  

  /**
   * Method of Domain enumeration
   */
  public enum Domain {
    PRIVATE("Privé"),
    PUBLIC("Public");

    private String domainLabel;

    /**
     * Costructor of enum Domain
     *
     * @param pDomainLabel
     */
    Domain(String pDomainLabel) {
      this.setDomainLabel(pDomainLabel);
    }

    /**
     * @return the domainLabel
     */
    public String getDomainLabel() {
      return domainLabel;
    }

    /**
     * @param domainLabel the domainLabel to set
     */
    public void setDomainLabel(String domainLabel) {
      this.domainLabel = domainLabel;
    }
  }

  /**
   * Alphabetical comparator of companies
   */
  public static Comparator<Company> companyComparator = new Comparator<Company>() {
    @Override
    public int compare(Company a1, Company a2) {
      return a1.getCompanyName().compareTo(a2.getCompanyName());
    }
  };

//--------------------------Entity attributs------------------------------------ 
  private Integer companyID = null;
  private String companyName;
  private Domain domain;
  private String streetNumber;
  private String streetName;
  private String postalcode;
  private String city;
  private String phoneNumber;
  private String emailAddress;
  private String comments;

//--------------------------Constructors----------------------------------------  
  /**
   * Company's empty constructor
   */
  public Company() {
  }

  /**
   * Company's constructor
   *
   * @param pCompanyName
   * @param pDomain
   * @param pStreetNumber
   * @param pStreetName
   * @param pPostalcode
   * @param pCity
   * @param pPhoneNumber
   * @param pEmailAddress
   * @throws EntryException
   */
  public Company(String pCompanyName, Domain pDomain, String pStreetNumber,
          String pStreetName, String pPostalcode, String pCity, String pPhoneNumber,
          String pEmailAddress) throws EntryException {

    setCompanyName(pCompanyName);
    setDomain(pDomain);
    setStreetNumber(pStreetNumber);
    setStreetName(pStreetName);
    setPostalcode(pPostalcode);
    setCity(pCity);
    setPhoneNumber(pPhoneNumber);
    setEmailAddress(pEmailAddress);
  }

//--------------------------Entity public methods-------------------------------
  /**
   * Method to write entity Company
   *
   * @return String of CompanyName
   */
  @Override
  public String toString() {
    return companyName;
  }

//--------------------------Entity protected methods----------------------------
//--------------------------Entity private methods------------------------------
  private void testNullAndEmpty(String pEntryString, String attributName) throws EntryException {
    if (pEntryString == null || pEntryString.trim().equals("")) {
      throw new EntryException(attributName + " can't be empty (recieved:\""
              + pEntryString + "\")");
    }
  }
//--------------------------Entity abstract methods-----------------------------
//-----------------Gettters and Setters of the Entity attributs-----------------

  /**
   * @return the companyID
   */
  public Integer getCompanyID() {
    return companyID;
  }

  /**
   * @return the companyName
   */
  public String getCompanyName() {
    return companyName;
  }

  /**
   * @return the streetNumber
   */
  public String getStreetNumber() {
    return streetNumber;
  }

  /**
   * @return the streetName
   */
  public String getStreetName() {
    return streetName;
  }

  /**
   * @return the postalcode
   */
  public String getPostalcode() {
    return postalcode;
  }

  /**
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * @return the emailAddress
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @return the comments
   */
  public String getComments() {
    return comments;
  }

  /**
   * @return the domain
   */
  public Domain getDomain() {
    return domain;
  }

  /**
   * @param pCompanyID the companyID to set
   */
  public void setCompanyID(Integer pCompanyID) throws EntryException {
    if (pCompanyID == null || pCompanyID < 1){
      throw new EntryException("Company ID can't be null or inferieur to 1, recieved: \""
      + pCompanyID + "\"");
    }
    this.companyID = pCompanyID;
  }

  /**
   * @param pCompanyName the pCompanyName to set Company name can't be empty
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setCompanyName(String pCompanyName) throws EntryException {
    testNullAndEmpty(pCompanyName, "Company name");
    this.companyName = pCompanyName;
  }

  /**
   * @param pStreetNumber the streetNumber to set Street number can't be empty
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setStreetNumber(String pStreetNumber) throws EntryException {
    testNullAndEmpty(pStreetNumber, "Street number");
    this.streetNumber = pStreetNumber;
  }

  /**
   * @param pStreetName the streetName to set Street name can't be empty
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setStreetName(String pStreetName) throws EntryException {
    testNullAndEmpty(pStreetName, "Street name");
    this.streetName = pStreetName;
  }

  /**
   * @param pPostalcode the postalcode to set Postalcode can't be empty
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setPostalcode(String pPostalcode) throws EntryException {
    testNullAndEmpty(pPostalcode, "Postalcode");
    this.postalcode = pPostalcode;
  }

  /**
   * @param pCity the city to set City can't be empty
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setCity(String pCity) throws EntryException {
    testNullAndEmpty(pCity, "City");
    this.city = pCity;
  }

  /**
   * @param pPhoneNumber the phoneNumber to set Phone number can't be empty and
   * has to be in correct format
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setPhoneNumber(String pPhoneNumber) throws EntryException {
    testNullAndEmpty(pPhoneNumber, "Phone number");
    if (!pPhoneNumber.matches("(?:(?:\\+|00)33|0)\\s*[1-9](?:[\\s.-]*\\d{2}){4}")) {
      throw new EntryException("Phone number is not in correct format : "+
              "ex. 06-26-65-68-89, "
              + "(only \".\", \"-\" and spaces are authorized as separators)" + 
              ", recieved: \"" + pPhoneNumber + "\"");
    }
    this.phoneNumber = pPhoneNumber;
  }

  /**
   * @param pEmailAddress the emailAddress to set Email address can't be empty
   * and has to be in correct format
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setEmailAddress(String pEmailAddress) throws EntryException {
    testNullAndEmpty(pEmailAddress, "Email address");
    if (!pEmailAddress.matches("[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}")) {
      throw new EntryException("Email address is not in correct format : (*@*.*** expected),"
              + " recieved: \"" + pEmailAddress + "\"");
    }
    this.emailAddress = pEmailAddress;
  }

  /**
   * @param pComments Empty comments set to null
   */
  public void setComments(String pComments) {
    if (pComments != null && pComments.trim().isEmpty()) {
      pComments = null;
    }
    this.comments = pComments;
  }

  /**
   * @param pDomain the domain to set Domain can't be empty
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setDomain(Domain pDomain) throws EntryException {
    if (pDomain == null || pDomain.toString().equals("")) {
      throw new EntryException("Domain can't be empty, recieved: \"" + 
              pDomain + "\"");
    }
    this.domain = pDomain;
  }
}
