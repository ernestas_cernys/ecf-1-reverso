/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cernys.reverso.entities;

import java.io.Serializable;
import com.cernys.reverso.exceptions.entities.EntryException;
import java.time.LocalDate;

/**
 *
 * @author CDA-25
 */
public class Prospect extends Company implements Serializable {

//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Class attributs-------------------------------------
//-----------------Getters and Setters of the Class attributs-------------------
//--------------------------Class methods---------------------------------------  
    
    /**
     * Method of enumeration if Prospect is interested
     */
    public enum Interest {
      YES ("Oui"),
      NO ("Non");
        
      private String interest;
        
      /**
       * @return the Interest
       */
      public String getInterestParametre() {
        return interest;
      }

      /**
       * @param pInterest the Interest to set
       */
      public void setInterestParametre(String pInterest) {
        this.interest = pInterest;
      }
        Interest (String pInterest){
          this.setInterestParametre(pInterest);
        }
    }
//--------------------------Attributes d’instance-------------------------------  
    private LocalDate prospectionDate;
    private Interest prospectInterest; 

//--------------------------Constructors----------------------------------------  
    /**
     * Empty constructor
     */
    public Prospect (){
        
    }
    
    /**
     * Prospect's constructor
     * @param pProspectionDate
     * @param pProspectInterest
     * @param pCompanyName
     * @param pDomain
     * @param pStreetNumber
     * @param pStreetName
     * @param pPostalcode
     * @param pCity
     * @param pPhoneNumber
     * @param pEmailAddress
     * @throws Exception 
     */
    public Prospect(LocalDate pProspectionDate, Interest pProspectInterest, String pCompanyName, Domain pDomain, String pStreetNumber,
            String pStreetName,String pPostalcode, String pCity, String pPhoneNumber,
            String pEmailAddress) throws Exception {
      super(pCompanyName, pDomain, pStreetNumber, pStreetName, pPostalcode, 
              pCity, pPhoneNumber, pEmailAddress);
      setProspectionDate(pProspectionDate);
      setProspectInterest(pProspectInterest);
    }
//--------------------------Entity public methods-------------------------------
//--------------------------Entity protected methods----------------------------
//--------------------------Entity private methods------------------------------
//--------------------------Entity abstract methods-----------------------------
//-----------------Gettters and Setters of the Entity attributs-----------------   

    /**
     * @return the prospectionDate
     */
    public LocalDate getProspectionDate() {
        return prospectionDate;
    }

    /**
     * @return the prospectInterest
     */
    public Interest getProjectInterest() {
        return prospectInterest;
    }

    /**
     * @param pProspectionDate the prospectionDate to set
     * Prospection date can't be empty and has to be in correct format
   * @throws com.cernys.reverso.exceptions.entities.EntryException
     */
    public void setProspectionDate(LocalDate pProspectionDate) throws EntryException{
      if (pProspectionDate == null){
          throw new EntryException("Prospection date can't be empty, recieved: \"" + 
                  pProspectionDate + "\"");
      }
      this.prospectionDate = pProspectionDate;
    }

    /**
     * @param pProspectIsInterested the prospectInterest to set
 Data if prospect is interested must be specified
   * @throws com.cernys.reverso.exceptions.entities.EntryException
     */
    public void setProspectInterest(Interest pProspectIsInterested) throws EntryException {
      if (pProspectIsInterested == null){
          throw new EntryException("Prospect Interest can't be null");
      }
      this.prospectInterest = pProspectIsInterested;
    }


  }
