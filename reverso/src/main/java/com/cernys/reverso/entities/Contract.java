/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.entities;

import com.cernys.reverso.exceptions.entities.EntryException;
import java.time.LocalDate;

/**
 *
 * @author CDA-25
 */
public class Contract {
//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Class attributs-------------------------------------
//-----------------Getters and Setters of the Class attributs-------------------
//--------------------------Class methods---------------------------------------  
//--------------------------Entity attributs------------------------------------ 

  private Integer contractID;
  private Integer clientID;
  private String contractTitle;
  private String contractDescription;
  private LocalDate contractStartDate;
  private LocalDate contractEndDate;
//--------------------------Constructors----------------------------------------  

  /**
   *
   */
  public Contract() {

  }

  /**
   * Contract constructor
   *
   * @param clientID
   * @param pContractTitle
   * @param pContractDescription
   * @param pContractStartDate
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public Contract(Integer clientID, String pContractTitle, String pContractDescription,
          LocalDate pContractStartDate) throws EntryException {
    setClientID(clientID);
    setContractTitle(pContractTitle);
    setContractDescription(pContractDescription);
    setContractStartDate(pContractStartDate);
  }
//--------------------------Entity public methods-------------------------------
//--------------------------Entity protected methods----------------------------
//--------------------------Entity private methods------------------------------

  private void testNullAndEmpty(String pEntryString, String attributName) throws EntryException {
    if (pEntryString == null || pEntryString.trim().equals("")) {
      throw new EntryException(attributName + " can't be empty (recieved:\""
              + pEntryString + "\")");
    }
  }
//--------------------------Entity abstract methods-----------------------------
//-----------------Gettters and Setters of the Entity attributs-----------------   

  /**
   * @return the contractID
   */
  public Integer getContractID() {
    return contractID;
  }

  /**
   * @return the contractTitle
   */
  public String getContractTitle() {
    return contractTitle;
  }

  /**
   * @return the contractDescription
   */
  public String getContractDescription() {
    return contractDescription;
  }

  /**
   * @return the contractStartDate
   */
  public LocalDate getContractStartDate() {
    return contractStartDate;
  }

  /**
   * @return the contractEndDate
   */
  public LocalDate getContractEndDate() {
    return contractEndDate;
  }

  /**
   * @param pContractID the contractID to set
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setContractID(Integer pContractID) throws EntryException {
    if (pContractID == null || pContractID < 1) {
      throw new EntryException("Contract ID can't be null or inferieur to 1, recieved: \""
              + pContractID + "\"");
    }
    this.contractID = pContractID;
  }

  /**
   * @param contractTitle the contractTitle to set
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setContractTitle(String contractTitle) throws EntryException {
    testNullAndEmpty(contractTitle, "Contract title");
    this.contractTitle = contractTitle;
  }

  /**
   * @param contractDescription the contractDescription to set
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setContractDescription(String contractDescription) throws EntryException {
    testNullAndEmpty(contractDescription, "Contract description");
    this.contractDescription = contractDescription;
  }

  /**
   * @param contractStartDate the contractStartDate to set
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setContractStartDate(LocalDate contractStartDate) throws EntryException {
    if (contractStartDate == null) {
      throw new EntryException("Contract start date can't be null");
    }
    this.contractStartDate = contractStartDate;
  }

  /**
   * @param contractEndDate the contractEndDate to set
   */
  public void setContractEndDate(LocalDate contractEndDate) {
    this.contractEndDate = contractEndDate;
  }

  /**
   * @return the clientID
   */
  public Integer getClientID() {
    return clientID;
  }

  /**
   * @param pClientID the clientID to set
   * @throws com.cernys.reverso.exceptions.entities.EntryException
   */
  public void setClientID(Integer pClientID) throws EntryException {
    if (pClientID == null || pClientID < 1) {
      throw new EntryException("Client ID can't be null or inferieur to 1, recieved: \""
              + pClientID + "\"");
    }
    this.clientID = pClientID;
  }

}
