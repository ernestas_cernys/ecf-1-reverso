/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.swing;

/**
 *
 * @author CDA-25
 */
public enum ActionType {
  CREATE ("Création"),
  MODIFY ("Modification"),
  DELETE ("Suppression"),
  DISPLAY ("Affichage");
  
  private String actionType;
  
  ActionType (String pActionType){
    this.setActionType(pActionType);
  }

  /**
   * @return the actionType
   */
  public String getActionType() {
    return actionType;
  }

  /**
   * @param actionType the actionType to set
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }
}
