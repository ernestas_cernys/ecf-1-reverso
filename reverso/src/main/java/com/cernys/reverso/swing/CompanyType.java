/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cernys.reverso.swing;

import com.cernys.reverso.entities.Client;
import com.cernys.reverso.entities.Contract;
import com.cernys.reverso.entities.Prospect;

/**
 *
 * @author CDA-25
 */
public enum CompanyType {
  CLIENT (Client.class, "Client"),
  PROSPECT (Prospect.class, "Prospect"),
  CONTRACT (Contract.class, "Contrat");
//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Class attributs-------------------------------------
//-----------------Getters and Setters of the Class attributs-------------------
//--------------------------Class methods---------------------------------------  
//--------------------------Entity attributs------------------------------------ 
  private Class companyTypeClass;
  private String companyTypeName;
//--------------------------Constructors----------------------------------------
  /**
   * Enum constructor 
   * @param pCompanyType Class of Company type (Client, Prospect...) 
   */
  CompanyType (Class pCompanyType, String pCompanyTypeName){
    this.setCompanyType(pCompanyType);
    this.setCompanyTypeName(pCompanyTypeName);
  }
//--------------------------Entity public methods-------------------------------
//--------------------------Entity protected methods----------------------------
//--------------------------Entity private methods------------------------------
//--------------------------Entity abstract methods-----------------------------
//-----------------Gettters and Setters of the Entity attributs-----------------

  /**
   * @return the companyTypeClass
   */
  public Class getCompanyTypeClass() {
    return companyTypeClass;
  }

  /**
   * @param companyType the companyTypeClass to set
   */
  public void setCompanyType(Class companyType) {
    this.companyTypeClass = companyType;
  }

  /**
   * @return the companyTypeName
   */
  public String getCompanyTypeName() {
    return companyTypeName;
  }

  /**
   * @param companyTypeName the companyTypeName to set
   */
  public void setCompanyTypeName(String companyTypeName) {
    this.companyTypeName = companyTypeName;
  }

}
