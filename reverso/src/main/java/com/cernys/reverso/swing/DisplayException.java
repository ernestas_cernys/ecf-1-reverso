/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.swing;

import com.cernys.reverso.exceptions.entities.EntryException;

/**
 *
 * @author CDA-25
 */
public class DisplayException extends EntryException {
  private Exception entryException;
  
  public DisplayException(String message) {
    super(message);
  }  
  
  public DisplayException (Exception pEntryException, String message){
    super(message);
    this.entryException = pEntryException;
  }
  
  public Exception getEntryException() {
    return this.entryException;
  }
  
}
