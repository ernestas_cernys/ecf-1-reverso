/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.exceptions.entities;

/**
 * Classe des exceptions de saisie herritant de la classe Exception
 * @author CDA-25
 */
public class EntryException extends Exception {

    public EntryException(String message) {
        super (message);
    }
    
}
