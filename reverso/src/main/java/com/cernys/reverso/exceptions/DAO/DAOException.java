/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.exceptions.DAO;

/**
 *
 * @author CDA-25
 */
public class DAOException extends Exception {

  public DAOException(String message) {
    super(message);
  }
}
