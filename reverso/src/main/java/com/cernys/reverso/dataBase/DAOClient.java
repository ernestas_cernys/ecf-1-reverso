/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.dataBase;

import com.cernys.reverso.exceptions.DAO.DAOException;
import com.cernys.reverso.entities.Client;
import com.cernys.reverso.entities.Company;
import com.cernys.reverso.exceptions.entities.EntryException;
import com.cernys.reverso.repository.ContractRepository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public class DAOClient extends DAOCommonPostgres implements DAOReverso<Client> {

//------------------------CONSTANTS---------------------------------------------
  private static final Logger LOGGER = LogManager.getLogger(DAOProspect.class.getName());
  private final String FIND_ALL = "findAllClients";
  private final String FIND = "findClient";
  private final String SAVE = "saveClient";
  private final String SAVE_NEW = "saveNewClient";
  private final String DELETE = "deleteClient";
  private String query;

//------------------------CONSTRUCTOR-------------------------------------------
  /**
   * DAOClient constructor
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  public DAOClient() throws DAOException {
    super();
  }

  //---------------------Method to read all Clients-----------------------------
  /**
   * Method to read all Clients
   *
   * @return List of clients
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  @Override
  public List<Company> findAll() throws DAOException {
    List<Company> clientList = new ArrayList<>();

    try {
      query = getQuery(FIND_ALL);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      LOGGER.info("Connected to Data base");
      ResultSet result = statement.executeQuery();
      while (result.next()) {
        clientList.add(getClient(result));
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return clientList;
  }

//---------------------Method to read a specific Client-------------------------
  /**
   * Method to read a Client
   *
   * @param clientID
   * @return Client
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   *
   */
  @Override
  public Client find(Integer clientID) throws DAOException {
    Client client = new Client();
    try {
      query = getQuery(FIND);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      statement.setInt(1, clientID);
      LOGGER.debug("Parametre ClientID value: " + clientID);
      System.out.println("Connected to Data base");
      ResultSet result = statement.executeQuery();
      result.next();
      client = getClient(result);
      client.setContracts(new ContractRepository().findAllbyClient(client));
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return client;
  }

  //----------------------Method to read a Client in ResultSet------------------
  /**
   * Method to read a Client in ResultSet
   *
   * @param result ResultSet
   * @return Client
   * @throws SQLException
   * @throws EntryException
   */
  private Client getClient(ResultSet result) throws SQLException, EntryException {
    Client client = new Client();
    Integer companyID = result.getInt(1);
    if (result.wasNull()) {
      companyID = null;
    }
    client.setCompanyID(companyID);
    client.setCompanyName(result.getString(2));
    if (Company.Domain.PRIVATE.getDomainLabel().equals(result.getString(3))) {
      client.setDomain(Company.Domain.PRIVATE);
    } else if (Company.Domain.PUBLIC.getDomainLabel().equals(result.getString(3))) {
      client.setDomain(Company.Domain.PUBLIC);
    }
    client.setStreetNumber(result.getString(4));
    client.setStreetName(result.getString(5));
    client.setPostalcode(result.getString(6));
    client.setCity(result.getString(7));
    client.setPhoneNumber(result.getString(8));
    client.setEmailAddress(result.getString(9));
    client.setComments(result.getString(10));
    client.setTurnover(result.getInt(11));
    client.setEmployeesCount(result.getInt(12));
    return client;
  }

  //----------------------Method to write a Client------------------------------
  /**
   * Method to write a Client
   *
   * @param client
   * @return Boolean if the client waswritten successfully
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  @Override
  public boolean save(Client client) throws DAOException {
    boolean result = false;
    try {
      // Verification if client is already in database 
      if (client.getCompanyID() == null) {
        query = getQuery(SAVE_NEW);
      } else {
        query = getQuery(SAVE);
      }
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(
              query,
              PreparedStatement.RETURN_GENERATED_KEYS);
      System.out.println("Connected to Data base");
      statement.setString(1, client.getCompanyName());
      statement.setString(2, client.getDomain().getDomainLabel());// null
      statement.setString(3, client.getStreetNumber());
      statement.setString(4, client.getStreetName());
      statement.setString(5, client.getPostalcode());
      statement.setString(6, client.getCity());
      statement.setString(7, client.getPhoneNumber());
      statement.setString(8, client.getEmailAddress());
      statement.setString(9, client.getComments());
      statement.setInt(10, client.getTurnover());
      statement.setInt(11, client.getEmployeesCount());
      if (client.getCompanyID() != null) {
        statement.setInt(12, client.getCompanyID());
      }
      int executeUpdate = statement.executeUpdate();
      ResultSet generatedKeys = statement.getGeneratedKeys();
      // set client_id generated by data base
      if (client.getCompanyID() == null && generatedKeys.next()) {
        client.setCompanyID(generatedKeys.getInt("client_id"));
      } else {
        throw new DAOException("Something went wrong, there was no generated "
                + "key while saving");
      }
      if (executeUpdate > 0) {
        result = true;
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return result;
  }

  /**
   * Method to delete client
   *
   * @param client
   * @return
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  @Override
  public boolean delete(Client client) throws DAOException {
    boolean result = false;
    try {
      query = getQuery(DELETE);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      statement.setInt(1, client.getCompanyID());
      System.out.println("Connected to Data base");
      int executeUpdate = statement.executeUpdate();
      if (executeUpdate > 0) {
        result = true;
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    }
    return result;
  }
}
