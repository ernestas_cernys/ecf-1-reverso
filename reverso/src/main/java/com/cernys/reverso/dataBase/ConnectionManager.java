/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.dataBase;

import com.cernys.reverso.exceptions.DAO.DAOException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public class ConnectionManager  {
  private static final Logger LOGGER = LogManager.getLogger(DAOProspect.class.getName());
  private static Connection connection;
  private static final Thread CLOSE_THREAD = new Thread() {
    @Override
    public void run() {
      if (ConnectionManager.connection != null) {
        try {
          ConnectionManager.connection.close();
          LOGGER.info("Connection closed");
        } catch (SQLException ex) {
          LOGGER.fatal("Error while closing connection " + "Message: " + ex.getMessage());
          LOGGER.fatal("StackTrace: "+ Arrays.toString(ex.getStackTrace()));
        }
      }
    }
  };

  static {
    Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
  }

  /**
   * Returns an opened Connection instance.IMPORTANT: Please do not close it yourself
   *
   * @return Connection
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  public static Connection getConnection() throws DAOException {
    Properties databaseProperties = new Properties();

    if (ConnectionManager.connection == null) {
      try (InputStream input = ConnectionManager.class.getClassLoader()
              .getResourceAsStream("databaseProperties")) {
        databaseProperties.load(input);
        ConnectionManager.connection = DriverManager.getConnection
        (
          (String) databaseProperties.get("url"), databaseProperties
        );
      } catch (IOException ieo) {
        LOGGER.fatal("Database properties file couldn't be loaded: " + 
                "Message: " + ieo.getMessage());
        throw new DAOException("Database properties file couldn't be loaded: ");
      } catch (SQLException sqle) {
        LOGGER.fatal("Message: " + sqle.getMessage() + 
              " [SQL error code : " + sqle.getSQLState() + " ]");
        LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
        LOGGER.debug("Error while reading properties file: ");
        throw new DAOException("Error while reading properties file: ");
      }

    }
    return ConnectionManager.connection;
  }
}
