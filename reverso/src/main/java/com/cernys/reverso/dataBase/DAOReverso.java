/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.dataBase;

import com.cernys.reverso.exceptions.DAO.DAOException;
import java.util.List;

/**
 *
 * @author CDA-25
 * @param <T>
 */
public interface DAOReverso<T> {
  public List findAll ()throws DAOException;
  public T find (Integer i)throws DAOException;
  public boolean save (T object) throws DAOException;
  public boolean delete (T object)throws DAOException;
}
