/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.dataBase;

import com.cernys.reverso.exceptions.DAO.DAOException;
import com.cernys.reverso.entities.Contract;
import com.cernys.reverso.exceptions.entities.EntryException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ecern
 */
public class DAOContract  {

  //------------------------CONSTANTS-------------------------------------------
  private static final Logger LOGGER = LogManager.getLogger(DAOContract.class.getName());
  private final String FIND_ALL = "findAllContracts";
  private final String FIND = "findContract";
  private final String SAVE = "saveContract";
  private final String SAVE_NEW = "saveNewContract";
  private final String DELETE = "deleteContract";

  //-----------------------Instance attribute-----------------------------------
  private String query;

  //------------------------Constructor-----------------------------------------
  /**
   * DAOContract constructor
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  public DAOContract() throws DAOException{
  }

  //---------------------Method to read all Contracts---------------------------
  /**
   * Method to read all contracts
   *
   * @return
   * @throws DAOException
   */
  @Override
  public List<Contract> findAll() throws DAOException {
    List<Contract> contractList = new ArrayList<>();
    try {
      query = getQuery(FIND_ALL);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      System.out.println("Connected to Data base");
      ResultSet result = statement.executeQuery();
      while (result.next()) {
        contractList.add(getContract(result));
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return contractList;
  }

//---------------------Method to read a specific contract-----------------------
  /**
   * Method to read a specific contract
   *
   * @param pContractID
   * @return Client
   * @throws DAOException
   */
  @Override
  public Contract find(Integer pContractID) throws DAOException {
    Contract contract = new Contract();
    try {
      query = getQuery(FIND);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      System.out.println("Connected to Data base");
      statement.setInt(1, pContractID);
      ResultSet result = statement.executeQuery();
      result.next();
      contract = getContract(result);
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return contract;
  }

  //----------------------Method to read a Contract in ResultSet------------------
  /**
   * Method to read a Client in ResultSet
   *
   * @param result ResultSet
   * @return Client
   * @throws SQLException
   * @throws EntryException
   */
  protected Contract getContract(ResultSet result) throws SQLException, EntryException {
    Contract contract = new Contract();
    Integer contractID = result.getInt(1);
    if (result.wasNull()) {
      contractID = null;
    }
    contract.setContractID(contractID);
    contract.setContractTitle(result.getString(2));
    contract.setContractDescription(result.getString(4));
    contract.setContractStartDate(result.getDate(5) == null ? null : result.getDate(5).toLocalDate());
    contract.setContractEndDate(result.getDate(5) == null ? null : result.getDate(5).toLocalDate());

    return contract;
  }

  //----------------------Method to write a Contract----------------------------
  /**
   * Method to write a Contract
   *
   * @param contract
   * @return Boolean if the contract was written successfully
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  @Override
  public boolean save(Contract contract) throws DAOException {
    boolean result = false;
    try {
      // Verification if client is already in database 
      if (contract.getContractID() == null) {
        query = getQuery(SAVE_NEW);
      } else {
        query = getQuery(SAVE);
      }
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(
              query,
              PreparedStatement.RETURN_GENERATED_KEYS);
      System.out.println("Connected to Data base");
      statement.setInt(1, contract.getClientID());
      statement.setString(2, contract.getContractTitle());
      statement.setString(3, contract.getContractDescription());
      // null values?????
      statement.setDate(4, Date.valueOf(contract.getContractStartDate()));
      statement.setDate(5, Date.valueOf(contract.getContractEndDate()));
      if (contract.getContractID() != null) {
        statement.setInt(12, contract.getContractID());
      }
      int executeUpdate = statement.executeUpdate();
      ResultSet generatedKeys = statement.getGeneratedKeys();
      // set client_id generated by data base
      if (contract.getContractID() == null && generatedKeys.next()) {
        contract.setContractID(generatedKeys.getInt("contract_id"));
      } else {
        throw new DAOException("Something went wrong, there was no generated "
                + "key while saving");
      }
      if (executeUpdate > 0) {
        result = true;
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return result;
  }

  //----------------------Method to write a Contract----------------------------
  /**
   * Method to delete contract
   *
   * @param contract
   * @return
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  @Override
  public boolean delete(Contract contract) throws DAOException {
    try {
      query = getQuery(DELETE);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      System.out.println("Connected to Data base");
      statement.setInt(1, contract.getContractID());
      int executeUpdate = statement.executeUpdate();
      if (executeUpdate > 0) {
        return true;
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    }
    return false;
  }
}
