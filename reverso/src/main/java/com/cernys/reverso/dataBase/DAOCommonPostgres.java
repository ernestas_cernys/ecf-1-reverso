/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.dataBase;

import com.cernys.reverso.exceptions.DAO.DAOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public abstract class DAOCommonPostgres {

  private static final Logger LOGGER = LogManager.getLogger(DAOCommonPostgres.class.getName());
  public static final Map<String, String> QUERIES = new HashMap<>();

  public DAOCommonPostgres() throws DAOException {
    DAOCommonPostgres.loadQueries();
  }

  /**
   *
   * @return @throws DAOException
   */
  public Connection getConnection() throws DAOException {
    return ConnectionManager.getConnection();
  }

  /**
   * Get fileNames from file
   *
   * @return List of fileNames
   * @throws DAOException if fileNames.txt is not read properly
   */
  private static List<String> getFileNames() throws DAOException {
    List<String> fileNames = new ArrayList<>();
    try (BufferedReader reader
            = new BufferedReader(
                    new FileReader(new File(DAOCommonPostgres.class
                            .getClassLoader()
                            .getResource("postgresql/fileNames.txt").toURI())))) {
      reader.lines().forEach(line -> fileNames.add(line));
    } catch (NullPointerException npe) {
      LOGGER.fatal("File not found: fileNames.txt");
      LOGGER.fatal("Message: " + npe.getMessage());
      LOGGER.fatal("StackTrace: " + Arrays.toString(npe.getStackTrace()));
      throw new DAOException("File not found: fileNames.txt");
    } catch (IOException ex) {
      LOGGER.fatal("Error while reading: fileNames.txt");
      LOGGER.fatal("Message: " + ex.getMessage());
      LOGGER.fatal("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while reading: fileNames.txt");
    } catch (URISyntaxException uex) {
      LOGGER.fatal("URL Syntax error while reading: fileNames.txt");
      LOGGER.fatal("Message: " + uex.getMessage());
      LOGGER.fatal("StackTrace: " + Arrays.toString(uex.getStackTrace()));
      throw new DAOException("URL Syntax error while reading: fileNames.txt");
    }
    return fileNames;
  }

  /**
   *
   * All query files should be in the resource directory, under the "postgresql"
   * directory
   *
   * @throws DAOException
   */
  private static void loadQueries() throws DAOException {
    for (String fileName : getFileNames()) {
      StringBuilder stringBuilder = new StringBuilder();
      String filePath = "postgresql/" + fileName + ".sql";
      try (BufferedReader reader
              = new BufferedReader(
                      new InputStreamReader(DAOCommonPostgres.class
                              .getClassLoader()
                              .getResourceAsStream(filePath)));) {
        reader.lines().forEach(line -> stringBuilder.append(line));
      } catch (FileNotFoundException fnfe) {
        LOGGER.fatal("File not found: " + fileName + ".sql");
        LOGGER.fatal("Message: " + fnfe.getMessage());
        LOGGER.fatal("StackTrace: " + Arrays.toString(fnfe.getStackTrace()));
        throw new DAOException("File not found: " + fileName + ".sql");
      } catch (IOException ex) {
        LOGGER.fatal("Error while reading: " + fileName + ".sql");
        LOGGER.fatal("Message: " + ex.getMessage());
        LOGGER.fatal("StackTrace: " + Arrays.toString(ex.getStackTrace()));
        throw new DAOException("Error while reading: " + fileName + ".sql");
      }
      QUERIES.put(fileName, stringBuilder.toString());
    }
  }

  protected String getQuery(String queryName) throws DAOException {
    // Lookup the query in the cache
    if (!QUERIES.keySet().contains(queryName)) {
      throw new DAOException("Requested query was not found: " + queryName);
    }
    return QUERIES.get(queryName);
  }
}
