/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.dataBase;

import com.cernys.reverso.exceptions.DAO.DAOException;
import com.cernys.reverso.entities.Company;
import com.cernys.reverso.entities.Prospect;
import com.cernys.reverso.exceptions.entities.EntryException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public class DAOProspect extends DAOCommonPostgres implements DAOReverso<Prospect> {

  private static final Logger LOGGER = LogManager.getLogger(DAOProspect.class.getName());

  //------------------------CONSTANTS-------------------------------------------
  private final String FIND_ALL = "findAllProspects";
  private final String FIND = "findProspect";
  private final String SAVE = "saveProspect";
  private final String SAVE_NEW = "saveNewProspect";
  private final String DELETE = "deleteProspect";

  //------------------------Instance attribute----------------------------------
  private String query;
  //------------------------Constructor-----------------------------------------

  /**
   * DAOProspect constructor
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  public DAOProspect() throws DAOException{
  }

  //---------------------Method to read all Prospects---------------------------
  /**
   * Method to read all Prospects
   *
   * @return List of Prospects
   * @throws DAOException
   */
  @Override
  public List<Company> findAll() throws DAOException {
    List<Company> prospectList = new ArrayList<>();
    try {
      query = getQuery(FIND_ALL);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      System.out.println("Connected to Data base");
      ResultSet result = statement.executeQuery();
      while (result.next()) {
        prospectList.add(getProspect(result));
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return prospectList;
  }
//---------------------Method to read a specific Prospect-------------------------

  /**
   * Method to read a Prospect
   *
   * @param prospectID
   * @return Prospect
   * @throws DAOException
   */
  @Override
  public Prospect find(Integer prospectID) throws DAOException {
    Prospect prospect = new Prospect();
    try {
      query = getQuery(FIND);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      System.out.println("Connected to Data base");
      statement.setInt(1, prospectID);
      ResultSet result = statement.executeQuery();
      result.next();
      prospect = getProspect(result);
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return prospect;
  }

  //----------------------Method to read a Prospect in ResultSet------------------
  /**
   * Method to read a Prospect in ResultSet
   *
   * @param result ResultSet
   * @return Prospect
   * @throws SQLException
   * @throws EntryException
   */
  private Prospect getProspect(ResultSet result) throws SQLException, EntryException {
    Prospect prospect = new Prospect();
    Integer companyID = result.getInt(1);
    if (result.wasNull()) {
      companyID = null;
    }
    prospect.setCompanyID(companyID);
    prospect.setCompanyName(result.getString(2));
    if (Company.Domain.PRIVATE.getDomainLabel().equals(result.getString(3))) {
      prospect.setDomain(Company.Domain.PRIVATE);
    } else if (Company.Domain.PUBLIC.getDomainLabel().equals(result.getString(3))) {
      prospect.setDomain(Company.Domain.PUBLIC);
    }
    prospect.setStreetNumber(result.getString(4));
    prospect.setStreetName(result.getString(5));
    prospect.setPostalcode(result.getString(6));
    prospect.setCity(result.getString(7));
    prospect.setPhoneNumber(result.getString(8));
    prospect.setEmailAddress(result.getString(9));
    prospect.setComments(result.getString(10));
    prospect.setProspectionDate(result.getDate(11).toLocalDate());
    if (Prospect.Interest.YES.getInterestParametre().equals(result.getString(12))) {
      prospect.setProspectInterest(Prospect.Interest.YES);
    } else if (Prospect.Interest.NO.getInterestParametre().equals(result.getString(12))) {
      prospect.setProspectInterest(Prospect.Interest.NO);
    }
    return prospect;
  }

  //----------------------Method to write a Prospect----------------------------
  /**
   * Method to write a Prospect
   *
   * @param prospect
   * @return Boolean if the prospect was written successfully
   * @throws DAOException
   */
  @Override
  public boolean save(Prospect prospect) throws DAOException {
    boolean result = false;
    // Verification if client is already in database 

    try {
      if (prospect.getCompanyID() == null) {
        query = getQuery(SAVE_NEW);
      } else {
        query = getQuery(SAVE);
      }
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(
              query,
              PreparedStatement.RETURN_GENERATED_KEYS);
      System.out.println("Connected to Data base");
      statement.setString(1, prospect.getCompanyName());
      statement.setString(2, prospect.getDomain().getDomainLabel());
      statement.setString(3, prospect.getStreetNumber());
      statement.setString(4, prospect.getStreetName());
      statement.setString(5, prospect.getPostalcode());
      statement.setString(6, prospect.getCity());
      statement.setString(7, prospect.getPhoneNumber());
      statement.setString(8, prospect.getEmailAddress());
      statement.setString(9, prospect.getComments());
      statement.setDate(10, Date.valueOf(prospect.getProspectionDate()));
      statement.setString(11, prospect.getProjectInterest().getInterestParametre());
      if (prospect.getCompanyID() != null) {
        statement.setInt(12, prospect.getCompanyID());
      }
      int executeUpdate = statement.executeUpdate();
      ResultSet generatedKeys = statement.getGeneratedKeys();
      // set client_id generated by data base
      if (prospect.getCompanyID() == null && generatedKeys.next()) {
        prospect.setCompanyID(generatedKeys.getInt("client_id"));
      } else {
        throw new DAOException("Something went wrong, there was no generated "
                + "key while saving");
      }
      if (executeUpdate > 0) {
        result = true;
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    } catch (EntryException ex) {
      LOGGER.error("Message: " + ex.getMessage());
      LOGGER.error("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("Error while setting database data");
    }
    return result;
  }

  //----------------------Method to delete a Prospect---------------------------
  /**
   * Method to delete
   *
   * @param prospect
   * @return
   * @throws com.cernys.reverso.exceptions.DAO.DAOException
   */
  @Override
  public boolean delete(Prospect prospect) throws DAOException {
    boolean result = false;
    try {
      query = getQuery(DELETE);
      Connection connection = getConnection();
      PreparedStatement statement = connection.prepareStatement(query);
      System.out.println("Connected to Data base");
      statement.setInt(1, prospect.getCompanyID());
      int executeUpdate = statement.executeUpdate();
      if (executeUpdate > 0) {
        result = true;
      }
    } catch (SQLException sqle) {
      LOGGER.fatal("Message: " + sqle.getMessage()
              + " [SQL error code : " + sqle.getSQLState() + " ]");
      LOGGER.fatal("StackTrace: " + Arrays.toString(sqle.getStackTrace()));
      LOGGER.debug("Error while preparing statement: " + query);
      throw new DAOException("Database connection error");
    }
    return result;
  }
}
