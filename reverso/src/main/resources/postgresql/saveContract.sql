INSERT INTO "Contract" (client_id, contract_title, contract_description, contract_start_date, 
	contract_end_date, contract_id)
VALUES (?, ?, ?, ?, ?, ?)
ON CONFLICT (contract_id) 
DO UPDATE 
SET client_id = EXCLUDED.client_id, 
	contract_title = EXCLUDED.contract_title,
	contract_description = EXCLUDED.contract_description, 
	contract_start_date = EXCLUDED.contract_start_date,
	contract_end_date = EXCLUDED.contract_end_date;