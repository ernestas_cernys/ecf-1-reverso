INSERT INTO "Client" (client_full_name, client_domain, client_street_number, 
	client_street_name, client_postalcode, client_city, client_phone_number,
	client_email_address, client_comments, client_turnover, 
	client_employees_count)
VALUES (?, ?::enum_company_domain, ?, ?, ?, ?, ?, ?, ?, ?, ?);

