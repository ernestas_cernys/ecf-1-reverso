INSERT INTO "Prospect" (prospect_full_name, prospect_domain, 
	prospect_street_number, prospect_street_name, prospect_postalcode,
	prospect_city, prospect_phone_number, prospect_email_address,
	prospect_comments, prospect_prospection_date, prospect_interest, prospect_id)
VALUES (?, ?::enum_company_domain, ?, ?, ?, ?, ?, ?, ?, ?, ?::enum_prospect_interest, ?)
ON CONFLICT (prospect_id) 
DO UPDATE 
SET prospect_full_name = EXCLUDED.prospect_full_name, 
	prospect_domain = EXCLUDED.prospect_domain,
	prospect_street_number = EXCLUDED.prospect_street_number, 
	prospect_street_name = EXCLUDED.prospect_street_name,
	prospect_postalcode = EXCLUDED.prospect_postalcode,
	prospect_city = EXCLUDED.prospect_city,
	prospect_phone_number = EXCLUDED.prospect_phone_number,
	prospect_email_address = EXCLUDED.prospect_email_address,
	prospect_comments = EXCLUDED.prospect_comments,
	prospect_prospection_date = EXCLUDED.prospect_prospection_date, 
	prospect_interest = EXCLUDED.prospect_interest;