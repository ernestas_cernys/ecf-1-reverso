INSERT INTO "Client" (client_full_name, client_domain, client_street_number, 
	client_street_name, client_postalcode, client_city, client_phone_number,
	client_email_address, client_comments, client_turnover, 
	client_employees_count, client_id)
VALUES (?, ?::enum_company_domain, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
ON CONFLICT (client_id)  
DO UPDATE 
SET client_full_name = EXCLUDED.client_full_name, 
	client_domain = EXCLUDED.client_domain,
	client_street_number = EXCLUDED.client_street_number, 
	client_street_name = EXCLUDED.client_street_name,
	client_postalcode = EXCLUDED.client_postalcode,
	client_city = EXCLUDED.client_city,
	client_phone_number = EXCLUDED.client_phone_number,
	client_email_address = EXCLUDED.client_email_address,
	client_comments = EXCLUDED.client_comments,
	client_turnover = EXCLUDED.client_turnover, 
	client_employees_count = EXCLUDED.client_employees_count;
