SELECT contract_id, client_id, contract_title, contract_description, contract_start_date, 
	contract_end_date 
FROM "Contract" 
 WHERE client_id = ?;
