INSERT INTO "Prospect" (prospect_full_name, prospect_domain, 
	prospect_street_number, prospect_street_name, prospect_postalcode,
	prospect_city, prospect_phone_number, prospect_email_address,
	prospect_comments, prospect_prospection_date, prospect_interest )
VALUES (?, ?::enum_company_domain, ?, ?, ?, ?, ?, ?, ?, ?, ?::enum_prospect_interest);
