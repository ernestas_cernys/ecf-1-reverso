package com.cernys.reverso.entities;

import com.cernys.reverso.exceptions.entities.EntryException;
import com.cernys.reverso.entities.Company.Domain;
import org.junit.jupiter.params.ParameterizedTest;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;



public class CompanyTest {
  //--------------------ConcreteEntityCreation----------------------------------
  class ConcreteCompany extends Company{};
  ConcreteCompany company = new ConcreteCompany();  
  
 //-----------------------setBusinessName--------------------------------------- 
  /**
   * Test of invalid CompanyName value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_CompanyName_Value_Should_Result_In_Exception (String pValue){
    assertThrows(EntryException.class, ()-> {company.setCompanyName(pValue);},
      "Correct BusinessName Value is not valid :" + "\"" + pValue + "\"");
  }
  
  /**
   * Test of valid CompanyName value in setter
   */
  @Test
  void correct_CompanyName_Value_Should_Throw_No_Exception (){
    String value = "AFPA Frouard";
    assertDoesNotThrow(()-> {company.setCompanyName(value);},
      "Incorrect BusinessName Value : \"" + value + "\"");
  }
  
  //-----------------------------setDomain--------------------------------------
  /**
   * Test of null Domain value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullSource
  void incorrect_Domain_Value_Should_Result_In_Exception (Domain pValue){
    assertThrows(EntryException.class, ()-> {company.setDomain(pValue);},
      "Correct Domain Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of correct Domain values in setter
   * @param pValue 
   */
  @ParameterizedTest
  @EnumSource (Domain.class)
  void correct_Domain_Value_Should_Throw_No_Exception (Domain pValue){
    assertDoesNotThrow(()-> {company.setDomain(pValue);},
       "Incorrect Domain Value : \"" + pValue + "\"");
  }
  
  //-----------------------setAddressStreetNumber-------------------------------
  /**
   * Test of incorrect StreetNumber values in setter
   * @param pValue  
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_StreetNumber_Input_Should_Result_In_Exception (String pValue){
      assertThrows(EntryException.class, () -> {
        company.setStreetNumber(pValue);},
          "Correct StreetNumber Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test for correct StreetNumber values in setter
   * @param pValue 
   */
  @Test
  void coorect_StreetNumber_Value_Should_Throw_No_Exception (){
    String pValue = "24A";
    assertDoesNotThrow(()-> {company.setStreetNumber(pValue);},
      "Incorrect StreetNumber Value : \"" + pValue + "\"");
  }
  
  //------------------------setAddressStreetName--------------------------------
  
  /**
   * Test of invalid StreetName value in setter
   * @param pValue  
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_StreetName_Value_Should_Result_In_Exception (String pValue){
    assertThrows(EntryException.class, ()-> {company.setStreetName(pValue);},
      "Correct StreetName Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid Street Name value in setter
   */
  @Test
  void correct_StreetName_Value_Should_Throw_No_Exception (){
    String pValue = "Avenue de l'AFPA";
    assertDoesNotThrow(()-> {company.setStreetName(pValue);},
      "Incorrect StreetName Value : \"" + pValue + "\"");
  }
  
  //---------------------------setAddressZipCode--------------------------------
  /**
   * Test of invalid Postalcode value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_Postalcode_Value_Should_Result_In_Exception (String pValue){
    assertThrows(EntryException.class, ()-> {company.setPostalcode(pValue);},
      "Correct ZipCode Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid Postalcode value in setter
   */
  @Test
  void correct_Postalcode_Value_Should_Throw_No_Exception (){
    String pValue = "67000";
    assertDoesNotThrow(()-> {company.setPostalcode(pValue);},
      "Incorrect ZipCode Value : \"" + pValue + "\"");
  }    
                                    
  //---------------------------setAddressCity-----------------------------------
  /**
   * Test of invalid City value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_City_Value_Should_Result_In_Exception (String pValue){
    assertThrows(EntryException.class, ()-> {company.setCity(pValue);},
      "Correct City Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid City value in setter
   */
  @Test
  void correct_City_Value_Should_Throw_No_Exception (){
    String pValue = "Strasbourg";
    assertDoesNotThrow(()-> {company.setCity(pValue);},
      "Incorrect City Value : \"" + pValue + "\"");                                          
  }
  
  //----------------------------setPhoneNumber----------------------------------
    /**
   * Test of invalid PhoneNumber value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {
    "",
    "   ",
    "\n",
    "\t",
    "Numéro",
    "0520357889 ",
    "88-15-45-45-45",
    "+468-45-45-46-45",
    "003506287889",
    "45455689121",
    "789456123"
  })
  void incorrect_PhoneNumber_Value_Should_Result_In_Exception (String pValue){
    assertThrows(EntryException.class, ()-> {company.setPhoneNumber(pValue);},
      "Correct PhoneNumber Value is not valid : \"" + pValue + "\"");
  }
  
  
  /**
   * Test of valid PhoneNumber value in setter
   * @param pValue
   */
  @ParameterizedTest
  @ValueSource (strings = {
  "0620678945",  
  "+33606894578",
  "06.20.67.89.45",
  "+336-55-56-73-76",
  "+336.56.89.45.78",
  "06 20 67 89 45",
  "+336 56 89 45 78"
  })
  void correct_PhoneNumber_Value_Should_Throw_No_Exception (String pValue){
    assertDoesNotThrow(()-> {company.setPhoneNumber(pValue);},
      "Incorrect PhoneNumber Value : \"" + pValue + "\"");                                          
  }
  
  //----------------------------setEmail----------------------------------------
    /**
   * Test of invalid Email value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {
    "",
    "   ",
    "\n",
    "\t",
    "Email",
    "Planetarium@airer.",
    "korona virus@smart.ltz",
    "@new.run",
    "Nevermind@tous abord",
    "neverever@",
    "under.world.is@real.Null.nevertheless",
    "45678@46543",
    "@"
  })
  void incorrect_Email_Value_Should_Result_In_Exception (String pValue){
    assertThrows(EntryException.class, ()-> {company.setEmailAddress(pValue);},
      "Correct Email Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid Email value in setter
   * @param pValue
   */
  @ParameterizedTest
  @ValueSource (strings = {
  "smart.beaver@march.fr",  
  "ide@netbeans.com",
  "me@offcourse.tr",
  "nulla@Nullelementumsemvitae.ca"
  })
  void correct_Email_Value_Should_Throw_No_Exception (String pValue){
    assertDoesNotThrow(()-> {company.setEmailAddress(pValue);},
      "Incorrect Email Value : \"" + pValue + "\"");                                          
  }
  
  //------------------------------setComments-----------------------------------
  
   /**
   * Test of empty and null Comments value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void empty_Or_Null_Comments_Value_Should_Return_Null (String pValue){
    company.setComments(pValue);
    assertNull(company.getComments(),
      "Not null or empty Comments value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of not empty or null Comments value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (strings = {"a  ", "Comment", "zero\n", " h \t"})
  void not_Null_Comments_Value_Should_Return_Value (String pValue){
    company.setComments(pValue);
    assertNotNull(company.getComments(),
      "Incorrect StreetNumber Value : \"" + pValue + "\"");
  }
}
