/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.reverso.entities;

import com.cernys.reverso.exceptions.entities.EntryException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author ecern
 */
public class ClientTest {
  Client client = new Client();
  
  //--------------------------setEmployeesCount---------------------------------
  /**
   * Test of incorrect headCount values in headCount setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (ints = {Integer.MIN_VALUE, -15, 0})
  void incorrect_HeadCount_Input_Should_Result_In_Exception (int pValue){
    assertThrows(EntryException.class, () -> {client.setEmployeesCount(pValue);},
      "Correct HeadCount value is not valid : " + "\"" + pValue + "\"");
  }
  /**
   * Test of correct headCount values in headCount setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (ints = {1, 15, Integer.MAX_VALUE/11})
  void coorect_HeadCount_Value_Should_Throw_No_Exception (int pValue){
    
    assertDoesNotThrow(()-> {
      client.setTurnover(Integer.MAX_VALUE);
      client.setEmployeesCount(pValue);
    },
      "Incorrect HeadCount value is not valid : " + "\"" + pValue + "\"");
  }
  
  //----------------------------setTurnover-------------------------------------
  /**
   * Test of incorrect turnover values in turnover setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (ints = {Integer.MIN_VALUE, -15, -1})
  void incorrect_Turnover_Input_Should_Result_In_Exception (
    int pValue){
    assertThrows(EntryException.class, () -> {client.setTurnover(pValue);},
      "Correct Turnover value is not valid : " + "\"" + pValue + "\"");
  }
  /**
   * Test for correct turnover values in turnover setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (ints = {11, Integer.MAX_VALUE})
  void coorect_Turnover_Value_Should_Throw_No_Exception (int pValue){
    assertDoesNotThrow(()-> {client.setTurnover(pValue);},
      "Incorrect Turnover value is not valid : " + "\"" + pValue + "\"");
  }
  
  //-------------------------turnoverHeadcountRatioIsValid----------------------
  /**
   * Test of incorrect Turnover and HeadCount Ratio in method turnoverHeadcountRatioIsValid
   * @param pTurnoverValue
   * @param pEmployeesCount 
   */
  @ParameterizedTest
  @CsvSource ({"12,-1", "11,0", "-1,1", "0,1", "10,1", "2147483647, 214748364"})
  void incorrect_Turnover_And_HeadCount_Ratio_Input_Should_Throw_Exception (
    int pTurnoverValue, int pEmployeesCount){
    assertThrows(EntryException.class, () -> {client.checkTurnover(pTurnoverValue, pEmployeesCount);},
      "Both correct values are not valid in test : " + "\"" + pTurnoverValue + "\", " + 
        "\"" + pEmployeesCount + "\"");
  }
  
   /**
   * Test of correct Turnover and HeadCount Ratio in method turnoverHeadcountRatioIsValid
   * @param pTurnoverValue
   * @param pHeadCountValue 
   */
  @ParameterizedTest
  @CsvSource ({"11,1",  "2147483647, 195225786"})
  void correct_Turnover_And_HeadCount_Ratio_Input_Should_Return_True (
    int pTurnoverValue, int pHeadCountValue){
    assertDoesNotThrow(() -> {client.checkTurnover(pTurnoverValue, pHeadCountValue);},
      "Either one of values is not valid in test : " + "\"" + pTurnoverValue + "\", " +
        "\"" + pHeadCountValue + "\"");
  }
  
}
  

